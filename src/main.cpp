#include <iostream>
#include <vector>
#include <fstream>
#include <filesystem>

using std::filesystem::path;
using std::filesystem::directory_entry;
using std::filesystem::directory_iterator;

int main() {
    auto thermal_path = path("/sys/class/thermal");
    auto thermal_de = directory_entry(thermal_path);
    if (!thermal_de.exists()) {
        std::cout << "thermal path doesnt exist!\n";
        std::exit(1);
    }

    std::vector<path> thermal_sources;

    // read data as if they were files
    std::ifstream sensor_file;

    // grab all entries from /sys/class/thermal
    for (auto const &f :  directory_iterator{ thermal_path }) {
        thermal_sources.emplace_back(f.path());

        sensor_file.open(f.path().string() + "/temp");
        std::string s;

        if (sensor_file.is_open()) {
            // get the value in degrees (49000 equals 49.000C)
            sensor_file >> s; 

            std::cout << s << "\n";

            sensor_file.close();
        }


    }

}
